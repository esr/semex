= semex
// SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
// SPDX-License-Identifier: BSD-2-Clause

An interactive exerciser for the semctl, semget, and semop calls, originally
from System V Unix.

This was originally written sometime in the dark and backward abysm of
time back in the 1980s.

If you are on a 32-bit machine or using a compiler where you can't count 
on a memory address being the same size as a long int, see the WARNING
comments in the code.

The latest version of this code lives at http://www.catb.org/~esr/semex/.


						Eric S. Raymond
						<esr@thyrsus.com>

# semex -- the semaphore exerciser

# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

VERS=$(shell sed -n <NEWS.adoc '/^[0-9]/s/:.*//p' | head -1)

semex: semex.c

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

install: semex.1 uninstall
	cp semex /usr/bin
	cp semex.1 /usr/share/man/man1/semex.1

uninstall:
	rm -f /usr/bin/semex /usr/share/man/man1/semex.1

clean:
	rm -f lex.yy.c semex *~ semex.tar.gz
	rm -f *.rpm semex-*.tar.gz semex.1 *.html

CPPCHECKOPTS =
cppcheck:
	cppcheck -DREVISION=$(VERS) $(CPPCHECKOPTS) semex.c

reflow:
	@clang-format --style="{IndentWidth: 8, UseTab: ForIndentation}" -i $$(find . -name "*.[ch]")

SOURCES = README.adoc NEWS.adoc COPYING semex.adoc Makefile semex.c semex-logo.png control

semex-$(VERS).tar.gz: $(SOURCES) semex.1
	@ls $(SOURCES) semex.1 | sed s:^:semex-$(VERS)/: >MANIFEST
	@(cd ..; ln -s semex semex-$(VERS))
	(cd ..; tar -czf semex/semex-$(VERS).tar.gz `cat semex/MANIFEST`)
	@(cd ..; rm semex-$(VERS))

dist: semex-$(VERS).tar.gz

release: semex-$(VERS).tar.gz semex.html
	shipper version=$(VERS) | sh -e -x

refresh: semex.html
	shipper -N -w version=$(VERS) | sh -e -x
